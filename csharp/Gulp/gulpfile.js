const { task, watch, series, src, dest }  = require('gulp'),
    gutil = require('gulp-util'),
    gulp_concat = require('gulp-concat'),
    fs = require('fs');

var folder = '../SpringChallenge2021';

devBuild = (process.env.NODE_ENV !== 'production');

task('watch', function (done) {
    watch('./' + folder + '/**/*.cs',  series(concat, reorder));
    done();
});

function reorder(cb) {
    gutil.log('Reordering lines!');
    fs.readFile("Merged.cs",
        "utf-8",
        function (err, data) {
            var lines = data
                .split('\n');
            var usings = lines
                .filter((l) => { return l.indexOf('using') === 0 })
                .map((l) => { return l.trim() });
            var uniquesUsings = usings.filter((item, pos) => { return usings.indexOf(item) === pos; });
            var code = lines.filter((l) => { return l.indexOf('using') !== 0 });
            var output = '//// ' + new Date().toTimeString() + '\n' + uniquesUsings.join('\n') + '\n' + code.join('\n');
            fs.writeFileSync('Merged.cs', output);
        });
    cb();
}

function concat(cb) {
    gutil.log('Concatenating files!');
    src(['./' + folder + '/**/*.cs', '!./' + folder + '/Properties/*.cs'])
        .pipe(gulp_concat('Merged.cs'))
        .pipe(dest('./'))
        .on('end', cb);
}
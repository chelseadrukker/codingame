﻿using System;
using System.Linq;
using System.Collections.Generic;

class Game
{
    public int day;
    public int nutrients;
    public List<Cell> board;
    public List<Action> possibleActions;
    public List<Tree> trees;
    public int mySun, opponentSun;
    public int myScore, opponentScore;
    public bool opponentIsWaiting;
    public int opponentChopped;

    public int DaysRemaining => 24 - day;
    public List<Tree> MyTrees => trees.Where(t => t.isMine).ToList();
    public List<Tree> MyHravestableTrees => MyTrees.Where(t => t.size == 3).ToList();

    public Game()
    {
        board = new List<Cell>();
        possibleActions = new List<Action>();
        trees = new List<Tree>();
    }

    public Action GetNextAction(Game game)
    {
        //WTF !!! WAIT CAUSES TO SLEEP UNTIL ROUND FINISHES
        //WTF ROUNDS ARE NOT ALWAYS CONTAINING 4 TURNS
        var turnsRemaining = game.DaysRemaining * 4;

        //TODO calculate from accessible fields
        //var treesToKeepBig = 2;
        var maxHarvestableTrees = 4;

        var maxSeeds = 1;
        var maxSmallTrees = 1;
        var maxMiddleTrees = 1;

        var deadCells = game.board.Count(c => c.richness == 0);

        Console.Error.WriteLine(game.opponentChopped);

        if (game.DaysRemaining > 1
            && (game.opponentChopped == 0 || game.mySun < 4 || !game.MyHravestableTrees.Any())
            && maxHarvestableTrees >= game.MyHravestableTrees.Count)
        {
            Console.Error.WriteLine("GROWING");
            var filteredActions = new List<Action>();
            filteredActions = filteredActions.Union(possibleActions.Where(a => a.type == Action.WAIT)).ToList();

            if (game.MyTrees.Count(t => t.size == 0) < maxSeeds)
                filteredActions = filteredActions.Union(possibleActions.Where(a => a.type == Action.SEED)).ToList();

            if (game.MyTrees.Count(t => t.size == 1) < maxSmallTrees)
                filteredActions = filteredActions.Union(possibleActions.Where(a => a.type == Action.GROW && game.MyTrees.Single(t => t.cellIndex == a.targetCellIdx).size == 0)).ToList();

            if(game.MyTrees.Count(t => t.size == 2) < maxMiddleTrees)
                filteredActions = filteredActions.Union(possibleActions.Where(a => a.type == Action.GROW && game.MyTrees.Single(t => t.cellIndex == a.targetCellIdx).size == 1)).ToList();

            filteredActions = filteredActions.Union(possibleActions.Where(a => a.type == Action.GROW && game.MyTrees.Single(t => t.cellIndex == a.targetCellIdx).size == 2)).ToList();

            var action = filteredActions
                    .OrderBy(a => a.type == Action.GROW ? 0 : a.type == Action.SEED ? 1 : 3)
                    .ThenByDescending(a => a.type == Action.GROW ? game.trees.Single(t => t.cellIndex == a.targetCellIdx).size : 0)
                    .ThenBy(a => a.targetArea)
                    .First();

            return action;
        }

        // harvest at the end
        else
        {
            Console.Error.WriteLine("CHOPPING");
            game.opponentChopped = game.opponentChopped == 0 ? 0 : game.opponentChopped - 1;

            Action action = possibleActions
                            .Where(a => a.type == Action.COMPLETE || a.type == Action.WAIT)
                            .OrderByDescending(a => a.type == Action.COMPLETE ? 1 : 0)
                            .ThenBy(a => a.targetArea)
                            .First();
            return action;
        }

    }
}

﻿class Tree
{
    public int cellIndex;
    public int size;
    public bool isMine;
    public bool isDormant;
    /// <summary>
    /// smaller better
    /// </summary>
    public int area;

    public Tree(int cellIndex, int size, bool isMine, bool isDormant)
    {
        this.cellIndex = cellIndex;
        this.size = size;
        this.isMine = isMine;
        this.isDormant = isDormant;
        this.area = cellIndex < 7 ? 0 : cellIndex < 19 ? 1 : 2;
    }
}

﻿using ReverseEngineering.Common;

namespace ReverseEngineering.GameObjects
{
    public class Passage
    {
        public Point Position { get; private set; }
        public string DisplayName => "•";

        public Passage(Point position)
        {
            Position = position;
        }
    }
}
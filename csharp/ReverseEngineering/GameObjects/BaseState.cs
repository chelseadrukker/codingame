﻿using System;

namespace ReverseEngineering.GameObjects
{
    public abstract class BaseState
    {
        protected Pacman pacman;

        public BaseState(Pacman pacman)
        {
            this.pacman = pacman;
        }
        public abstract Type Tick();
    }
}
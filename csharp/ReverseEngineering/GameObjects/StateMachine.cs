﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ReverseEngineering.GameObjects
{
    public class StateMachine
    {
        private Dictionary<Type, BaseState> states;

        public BaseState CurrentState { get; private set; }
        public event Action<BaseState> OnStateChanged;
        public StateMachine(Dictionary<Type, BaseState> states)
        {
            this.states = states;
        }


        public void Update() {
            if (CurrentState == null) {
                CurrentState = states.Values.First();
            }

            var nextState = CurrentState.Tick();

            if (nextState == null)
                return;

            SwitchToNewState(nextState);
            Update();
        }

        private void SwitchToNewState(Type nextState)
        {
            CurrentState = states[nextState];
            OnStateChanged?.Invoke(CurrentState);
        }
    }
}
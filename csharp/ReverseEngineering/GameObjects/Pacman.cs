﻿using System;
using System.Collections.Generic;

namespace ReverseEngineering.GameObjects
{
    public class Pacman
    {
        public StateMachine StateMachine;
        public Pacman()
        {
            var states = new Dictionary<Type, BaseState>() {
                { typeof(CollectState), new CollectState(this) },
                { typeof(EscapeState), new EscapeState(this) },
            };
            StateMachine = new StateMachine(states);
        }
    }
}
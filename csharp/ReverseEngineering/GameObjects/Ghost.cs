﻿using ReverseEngineering.Common;

namespace ReverseEngineering.GameObjects
{
    public class Ghost
    {
        public Point Position { get; private set; }
        public string DisplayName { get; private set; }

        public Ghost(Point position, string displayName)
        {
            Position = position;
            DisplayName = displayName;
        }
    }
}
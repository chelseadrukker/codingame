﻿using System;

namespace ReverseEngineering.GameObjects
{
    public class EscapeState : BaseState
    {
        private Pacman pacman;

        public EscapeState(Pacman pacman) : base(pacman)
        {
            this.pacman = pacman;
        }

        public override Type Tick()
        {
            if (Map.Instance.NearestGhostsDistance > GameSettings.Instance.DangerZone)
                return typeof(CollectState);

            // make move
            Console.WriteLine(Map.Instance.EscapeNeighbour.Move);
            return null;
        }
    }
}
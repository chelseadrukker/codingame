﻿using System;
using System.Linq;

namespace ReverseEngineering.GameObjects
{
    public class CollectState : BaseState
    {
        private readonly Pacman pacman;

        public CollectState(Pacman pacman) : base(pacman)
        {
            this.pacman = pacman;
        }

        public override Type Tick()
        {
            if(Map.Instance.NearestGhostsDistance <= GameSettings.Instance.DangerZone)
                return typeof(EscapeState);

            // make move
            Console.WriteLine(Map.Instance.Neighbours.Where(n => !n.IsWall).First().Move);
            return null;
        }
    }
}
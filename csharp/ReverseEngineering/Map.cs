﻿using ReverseEngineering.Common;
using ReverseEngineering.GameObjects;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ReverseEngineering
{
    public sealed class Map
    {
        private static Map instance = null;
        private string neighbourInfo;

        public List<Neighbour> Neighbours { get; private set; }
        public List<Passage> Passages { get; private set; }
        public List<Ghost> Ghosts { get; private set; }
        public List<Point> Visited { get; private set; }
        public Point PacmanPosition { get; private set; }

        private Map()
        {
            Neighbours = new List<Neighbour>();
            Ghosts = new List<Ghost>();
            Passages = new List<Passage>();
            Visited = new List<Point>();
        }

        public static Map Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Map();
                }
                return instance;
            }
        }

        public Ghost NearestGhost { get; private set; }
        public int NearestGhostsDistance { get; private set; }
        public Neighbour EscapeNeighbour { get; private set; }

        public void Update()
        {
            // DO NOT CHANGE THE ORDER
            ReadNeighbours();
            ReadCharacters();
            CreateNeighbours();
            // DO NOT CHANGE THE ORDER

            AddVisited();
            AddPassages();

            Calculations();
        }

        private void Calculations()
        {
            var nearestGhostWithDistance = Ghosts
                .Select(g => new { Ghost = g, Distance = g.Position.DistanceTo(PacmanPosition) })
                .OrderBy(g => g.Distance)
                .First();
            NearestGhost = nearestGhostWithDistance.Ghost;
            NearestGhostsDistance = nearestGhostWithDistance.Distance;

            var neighboursWithoutNearestToGhost = Neighbours
                .Where(n => !n.IsWall)
                .Select(n => new { Neighbour = n, Distance = n.Position.DistanceTo(NearestGhost.Position) })
                .OrderBy(g => g.Distance)
                .Skip(1);
            var firstNotVisited = neighboursWithoutNearestToGhost.FirstOrDefault(n => !Visited.Contains(n.Neighbour.Position));
            var lastNeighbour = neighboursWithoutNearestToGhost.LastOrDefault();
            EscapeNeighbour = firstNotVisited != null ? firstNotVisited.Neighbour : lastNeighbour != null ? lastNeighbour.Neighbour : Neighbours.Where(n => !n.IsWall).First();
        }

        private void AddVisited()
        {
            Visited.Add(PacmanPosition);
        }

        private void ReadNeighbours()
        {
            neighbourInfo = $"{Console.ReadLine()}{Console.ReadLine()}{Console.ReadLine()}{Console.ReadLine()}";
        }

        private void CreateNeighbours()
        {
            Neighbours = new List<Neighbour>();
            Neighbours.Add(new Neighbour(neighbourInfo[0].ToString(), new Point(PacmanPosition.X, PacmanPosition.Y -1 ), "C")); // UP
            Neighbours.Add(new Neighbour(neighbourInfo[1].ToString(), new Point(PacmanPosition.X + 1, PacmanPosition.Y), "A")); // RIGHT
            Neighbours.Add(new Neighbour(neighbourInfo[2].ToString(), new Point(PacmanPosition.X, PacmanPosition.Y + 1), "D")); // DOWN
            Neighbours.Add(new Neighbour(neighbourInfo[3].ToString(), new Point(PacmanPosition.X - 1, PacmanPosition.Y), "E")); // LEFT
        }

        private void ReadCharacters()
        {
            Ghosts = new List<Ghost>();
            for (var id = 0; id < GameSettings.Instance.NumberOfGhosts - 1; id++)
            {
                Ghosts.Add(new Ghost(ReadPoint(), id.ToString()));
            }
            PacmanPosition = ReadPoint();
        }

        private void AddPassages()
        {
            var positions = Ghosts.Select(g => g.Position).Append(PacmanPosition);
            foreach (var position in positions)
            {
                if (!Passages.Any(p => p.Position == position))
                    Passages.Add(new Passage(position));
            }
        }

        private Point ReadPoint()
        {
            var position = Console.ReadLine().Split(' ');
            return new Point(int.Parse(position[0]), int.Parse(position[1]));
        }
    }
}
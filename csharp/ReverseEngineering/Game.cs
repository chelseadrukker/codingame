﻿using ReverseEngineering.GameObjects;
using System;

namespace ReverseEngineering
{
    public class Game
    {
        public Game()
        {
            GameSettings.Instance.Height = int.Parse(Console.ReadLine());
            GameSettings.Instance.Width = int.Parse(Console.ReadLine());
            GameSettings.Instance.NumberOfGhosts = int.Parse(Console.ReadLine());
        }

        public void Play()
        {
            var pacman = new Pacman();
            while (true)
            {
                Map.Instance.Update();
                new Printer().Print();
                pacman.StateMachine.Update();
            }
        }
    }
}
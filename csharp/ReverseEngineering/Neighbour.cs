﻿using ReverseEngineering.Common;

namespace ReverseEngineering
{
    public class Neighbour
    {
        public bool IsWall { get; private set; }
        public Point Position { get; private set; }
        public string Move { get; private set; }

        public Neighbour(string cellType, Point position, string move)
        {
            IsWall = cellType == "#";
            Position = position;
            Move = move;
        }
    }
}
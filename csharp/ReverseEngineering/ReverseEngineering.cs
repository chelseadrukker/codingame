﻿namespace ReverseEngineering
{
    public class ReverseEngineering
    {
        static void Main(string[] args)
        {
            var game = new Game();
            game.Play();
        }
    }
}
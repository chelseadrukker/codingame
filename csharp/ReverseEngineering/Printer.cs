﻿using ReverseEngineering.Common;
using System;
using System.Linq;

namespace ReverseEngineering
{
    public class Printer {
        public void Print() {
            Console.Error.WriteLine(" " + (Map.Instance.Neighbours[0].IsWall ? "#" : "_"));
            Console.Error.WriteLine((Map.Instance.Neighbours[3].IsWall ? "#" : "_") + " " + (Map.Instance.Neighbours[1].IsWall ? "#" : "_"));
            Console.Error.WriteLine(" " + (Map.Instance.Neighbours[2].IsWall ? "#" : "_"));
            Console.Error.WriteLine();

            for (var y = 0; y < GameSettings.Instance.Height; y++)
            {
                for (var x = 0; x < GameSettings.Instance.Width; x++)
                {
                    var current = new Point(x, y);
                    if (Map.Instance.PacmanPosition == current)
                        Console.Error.Write("<");
                    else if (Map.Instance.Ghosts.Any(g => g.Position == current))
                        Console.Error.Write(Map.Instance.Ghosts.First(g => g.Position == current).DisplayName);
                    else if (Map.Instance.Visited.Any(p => p == current))
                        Console.Error.Write(" ");
                    else if (Map.Instance.Passages.Any(p => p.Position == current))
                        Console.Error.Write(Map.Instance.Passages.Single(p => p.Position == current).DisplayName);
                    else Console.Error.Write("#");
                }
                Console.Error.WriteLine();
            }
        }
    }
}
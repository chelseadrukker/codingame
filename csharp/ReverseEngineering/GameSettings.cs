﻿namespace ReverseEngineering
{
    public sealed class GameSettings
    {
        private GameSettings()
        {
            DangerZone = 3;
        }
        private static GameSettings instance = null;
        public static GameSettings Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new GameSettings();
                }
                return instance;
            }
        }

        public int Height { get; set; }
        public int Width { get; set; }
        public int NumberOfGhosts { get; set; }
        public int DangerZone { get; private set; }
    }
}
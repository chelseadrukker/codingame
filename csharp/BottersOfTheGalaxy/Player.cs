﻿namespace BottersOfTheGalaxy
{
    internal class Player
    {
        public int Gold { get; }

        public Player(int gold)
        {
            Gold = gold;
        }
    }
}
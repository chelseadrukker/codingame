﻿using BottersOfTheGalaxy.Units;

namespace BottersOfTheGalaxy
{
    internal class Point
    {
        public int X { get; private set; }

        public int Y { get; private set; }

        public Point(int x, int y)
        {
            X = x;
            Y = y;
        }

        public override string ToString()
        {
            return $"{X} {Y}";
        }

        public double Distance(Point point)
        {
            return Utilities.GetDistanceWithoutSqrt(this, point);
        }
    }
}
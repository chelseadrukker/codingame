﻿namespace BottersOfTheGalaxy
{
    internal class Item
    {
        public string ItemName { get; set; }
        public int ItemCost { get; set; }
        public int Damage { get; set; }
        public int Health { get; set; }
        public int MaxHealth { get; set; }
        public int Mana { get; set; }
        public int MoveSpeed { get; set; }
        public bool IsPotion { get; set; }
        public int MaxMana { get; set; }
        public int ManaRegeneration { get; set; }
    }
}
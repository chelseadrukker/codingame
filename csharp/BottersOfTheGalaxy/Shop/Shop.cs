﻿using System.Collections.Generic;

namespace BottersOfTheGalaxy
{
    internal class Shop
    {
        public List<Item> Items = new List<Item>();

        public Shop(List<string[]> items)
        {
            ReadItems(items);
        }

        private void ReadItems(List<string[]> items)
        {
            foreach (var shopItem in items)
            {
                var item = new Item
                {
                    ItemName =
                        shopItem
                            [0], // contains keywords such as BRONZE, SILVER and BLADE, BOOTS connected by "_" to help you sort easier
                    ItemCost =
                        int.Parse(shopItem[1]), // BRONZE items have lowest cost, the most expensive items are LEGENDARY
                    Damage =
                        int.Parse(shopItem[2]), // keyword BLADE is present if the most important item stat is damage
                    Health = int.Parse(shopItem[3]),
                    MaxHealth = int.Parse(shopItem[4]),
                    Mana = int.Parse(shopItem[5]),
                    MaxMana = int.Parse(shopItem[6]),
                    MoveSpeed =
                        int.Parse(shopItem[7]), // keyword BOOTS is present if the most important item stat is moveSpeed
                    ManaRegeneration = int.Parse(shopItem[8]),
                    IsPotion = int.Parse(shopItem[9]) != 0 // 0 if it's not instantly consumed    
                };
                Items.Add(item);
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;

namespace BottersOfTheGalaxy
{
    internal class Game
    {
        private const int WaveSpawnInterval = 15;

        private int _myTeamId;
        private Map.Map _map;
        private Shop _shop;
        private Turn _currentTurn;

        public Game()
        {
            _myTeamId = int.Parse(Console.ReadLine());
            _map = new Map.Map(ReadBushesAndMonsterSpawns());
            _shop = new Shop(ReadShopItems());
        }

        private static List<string[]> ReadBushesAndMonsterSpawns()
        {
            var bushesAndMonsterSpawns = new List<string[]>();
            for (var i = 0; i < int.Parse(Console.ReadLine()); i++)
                bushesAndMonsterSpawns.Add(Console.ReadLine().Split(' '));
            return bushesAndMonsterSpawns;
        }

        private static List<string[]> ReadShopItems()
        {
            var items = new List<string[]>();
            var itemCount = int.Parse(Console.ReadLine());
            for (var i = 0; i < itemCount; i++)
                items.Add(Console.ReadLine().Split(' '));
            return items;
        }


        public void Play()
        {
            // game loop
            while (true)
            {
                _currentTurn = new Turn(_myTeamId);

                // If roundType has a negative value then you need to output a Hero name, such as "DEADPOOL" or "VALKYRIE".
                // Else you need to output roundType number of any valid action, such as "WAIT" or "ATTACK unitId"
                var action = IsInitializationTurn(_currentTurn.RoundType) ? HeroType.Ironman : _currentTurn.MyHero().TakeAction(_currentTurn, _shop);
                Action(action);
            }
        }

        private static bool IsInitializationTurn(int roundType)
        {
            return roundType < 0;
        }

        private void Action(string action)
        {

            Console.WriteLine(action);
        }
    }
}
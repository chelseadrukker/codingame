﻿using System;

namespace BottersOfTheGalaxy.Units
{
    internal class Utilities
    {
        public static double GetDistanceWithoutSqrt(Point source, Point target)
        {
            return Math.Pow(source.X - target.X, 2) + Math.Pow(source.Y - target.Y, 2);
        }
    }
}
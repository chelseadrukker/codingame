﻿using System;

namespace BottersOfTheGalaxy.Units
{
    internal class EntityFactory
    {
        public Entity Get(string[] inputs)
        {
            var unitType = inputs[2];
            switch (unitType)
            {
                case EntityType.Hero:
                    return new Hero(inputs);
                case EntityType.Tower:
                    return new Tower(inputs);
                case EntityType.Unit:
                    return new Unit(inputs);
                case EntityType.Groot:
                    return new Groot(inputs);
                default:
                    throw new Exception("Unknown entity type!");
            }
        }
    }
}
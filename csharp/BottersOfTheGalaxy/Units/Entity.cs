﻿using System;

namespace BottersOfTheGalaxy.Units
{
    internal class Entity
    {
        private int _countDown1;
        private int _countDown2;
        private int _countDown3;
        private int _goldValue;
        private string _heroType;
        private int _isVisible;
        private int _itemsOwned;
        private int _mana;
        private int _manaRegeneration;
        private int _maxHealth;
        private int _maxMana;
        private int _movementSpeed;
        private int _shield;
        private int _stunDuration;
        private readonly int _x;
        private readonly int _y;

        public Entity(string[] inputs)
        {
            Id = int.Parse(inputs[0]);
            Team = int.Parse(inputs[1]);
            UnitType = inputs[2]; // UNIT, HERO, TOWER, can also be GROOT from wood1
            _x = int.Parse(inputs[3]);
            _y = int.Parse(inputs[4]);
            AttackRange = int.Parse(inputs[5]);
            Health = int.Parse(inputs[6]);
            _maxHealth = int.Parse(inputs[7]);
            _shield = int.Parse(inputs[8]); // useful in bronze
            AttackDamage = int.Parse(inputs[9]);
            _movementSpeed = int.Parse(inputs[10]);
            _stunDuration = int.Parse(inputs[11]); // useful in bronze
            _goldValue = int.Parse(inputs[12]);
            _countDown1 = int.Parse(inputs[13]); // all countDown and mana variables are useful starting in bronze
            _countDown2 = int.Parse(inputs[14]);
            _countDown3 = int.Parse(inputs[15]);
            _mana = int.Parse(inputs[16]);
            _maxMana = int.Parse(inputs[17]);
            _manaRegeneration = int.Parse(inputs[18]);
            _heroType = inputs[19]; // DEADPOOL, VALKYRIE, DOCTOR_STRANGE, HULK, IRONMAN
            _isVisible = int.Parse(inputs[20]); // 0 if it isn't
            _itemsOwned = int.Parse(inputs[21]); // useful from wood1
        }

        public int Id { get; }
        public int Team { get; }
        public string UnitType { get; }
        public double AttackRange { get; }
        public double Health { get; }
        public double AttackDamage { get; }
        public Point Position => new Point(_x, _y);

        public bool CanAttack(Entity enemy)
        {
            return Utilities.GetDistanceWithoutSqrt(Position, enemy.Position) < Math.Pow(enemy.AttackRange, 2);
        }
    }
}
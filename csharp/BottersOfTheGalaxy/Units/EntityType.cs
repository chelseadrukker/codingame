﻿namespace BottersOfTheGalaxy.Units
{
    internal class EntityType
    {
        public const string Hero = "HERO";
        public const string Unit = "UNIT";
        public const string Tower = "TOWER";
        public const string Groot = "GROOT";
    }
}
﻿using System.Linq;

namespace BottersOfTheGalaxy.Units
{
    internal class Hero : Entity
    {
        public Hero(string[] inputs) : base(inputs)
        {
        }

        public string TakeAction(Turn turn, Shop shop)
        {
            var me = turn.MyHero();
            if (turn.Enemies().Any(e => e.CanAttack(me)))
                return MoveTowardsOwnTower(turn);
            var cheapestItem = shop.Items.FirstOrDefault(i => i.IsPotion && i.Health > 0 && i.ItemCost < turn.Me.Gold);
            if (cheapestItem != null)
                return Buy(cheapestItem.ItemName);
            return Attack(turn.Enemies().OfType<Unit>().OrderBy(u => u.Position.Distance(Position)).ThenBy(u => u.Health).First().Id);
        }

        private string MoveTowardsOwnTower(Turn turn)
        {
            return Move(turn.MyTower().Position);
        }

        private string Wait()
        {
            return "WAIT";
        }

        private string Move(Point point)
        {
            return $"MOVE {point.X} {point.Y}";
        }

        private string Attack(int unitId)
        {
            return $"ATTACK {unitId}";
        }

        private string AttackNearest(string unitType)
        {
            return $"ATTACK_NEAREST {unitType}";
        }

        private string MoveAttack(int x, int y, int unitId)
        {
            return $"MOVE_ATACK {x} {y} {unitId}";
        }

        private string Buy(string itemName)
        {
            return $"BUY {itemName}";
        }

        private string Sell(string itemName)
        {
            return $"SELL {itemName}";
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using BottersOfTheGalaxy.Units;

namespace BottersOfTheGalaxy
{
    internal class Turn
    {
        private readonly int _myTeam;
        public Player Me { get; private set; }
        public Player Enemy { get; private set; }
        public int RoundType { get; private set; }
        private List<Entity> Entities { get; } = new List<Entity>();

        public Turn(int myTeam)
        {
            _myTeam = myTeam;
            ReadTurnInformation();
        }

        private void ReadTurnInformation()
        {
            Me = new Player(int.Parse(Console.ReadLine()));
            Enemy = new Player(int.Parse(Console.ReadLine()));
            RoundType = int.Parse(Console.ReadLine()); // a positive value will show the number of heroes that await a command
            ReadEntities();
        }

        private void ReadEntities()
        {
            var entityCount = int.Parse(Console.ReadLine());
            for (var i = 0; i < entityCount; i++)
            {
                var inputs = Console.ReadLine().Split(' ');
                Entities.Add(new EntityFactory().Get(inputs));
            }
        }

        public IEnumerable<Entity> Enemies()
        {
            return Entities.Where(e => e.Team != _myTeam);
        }

        public Hero MyHero()
        {
            return Entities.OfType<Hero>().Single(e => e.Team == _myTeam);
        }

        public Tower MyTower()
        {
            return Entities.OfType<Tower>().Single(e => e.Team == _myTeam);
        }
    }
}
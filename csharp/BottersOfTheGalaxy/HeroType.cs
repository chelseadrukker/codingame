﻿namespace BottersOfTheGalaxy
{
    internal class HeroType
    {
        public const string Deadpool = "DEADPOOL";
        public const string DoctorStrange = "DOCTOR_STRANGE";
        public const string Hulk = "HULK";
        public const string Ironman = "IRONMAN";
        public const string Valkyrie = "VALKYRIE";
    }
}
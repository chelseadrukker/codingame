﻿using System.Collections.Generic;

namespace BottersOfTheGalaxy.Map
{
    internal class Map
    {
        public const int Width = 1920;
        public const int Height = 750;

        public List<Bush> Bushes = new List<Bush>();
        public List<MonsterSpawn> MonsterSpawns = new List<MonsterSpawn>();

        public Map(List<string[]> bushesAndSpawns)
        {
            ReadObjects(bushesAndSpawns);
        }

        private void ReadObjects(List<string[]> bushesAndSpawns)
        {
            foreach (var mapObject in bushesAndSpawns)
            {
                var entityType = mapObject[0]; // BUSH, from wood1 it can also be SPAWN
                var x = int.Parse(mapObject[1]);
                var y = int.Parse(mapObject[2]);
                var radius = int.Parse(mapObject[3]);

                if (entityType == "BUSH")
                    Bushes.Add(new Bush(x, y, radius));
                else
                    MonsterSpawns.Add(new MonsterSpawn(x, y, radius));
            }
        }
    }
}
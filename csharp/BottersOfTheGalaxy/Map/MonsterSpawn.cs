﻿namespace BottersOfTheGalaxy.Map
{
    internal class MonsterSpawn
    {
        private readonly int _x;
        private readonly int _y;
        private readonly int _radius;

        public MonsterSpawn(int x, int y, int radius)
        {
            _x = x;
            _y = y;
            _radius = radius;
        }
    }
}
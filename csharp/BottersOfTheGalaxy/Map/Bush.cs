﻿namespace BottersOfTheGalaxy.Map
{
    internal class Bush
    {
        private readonly int _x;
        private readonly int _y;
        private readonly int _radius;

        public Bush(int x, int y, int radius)
        {
            _x = x;
            _y = y;
            _radius = radius;
        }
    }
}
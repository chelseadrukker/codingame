﻿namespace MeanMax
{
    class Constants
    {
        public const int SKILL_RANGE = 2000;
        public const int MAX_TURNS = 10000;
        public const int GRENADE_COST = 60;
        public const int OIL_COST = 30;
        public const int TAR_COST = 30;

        //// reaper variables
        public const int REAPER_FAKE_TURN_INTERVAL = 10;
        public const int REAPER_FAKE_TURN_DURATION = 2;

        public const bool REAPER_USE_SKILL = false;
        public const int REAPER_DOOF_SKILL_RANGE = 150;

        //// destroyer variables
        public const bool DESTROYER_USE_SKILL = false;

        //// doof variables
        public const int DOOF_SKILL_FAKE = 1;
        public const bool DOOF_USE_SKILL = true;

    }
}

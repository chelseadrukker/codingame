﻿using MeanMax.Commanders;
using MeanMax.Units;

namespace MeanMax
{
    public class Game
    {
        private readonly MapParser mapParser;

        public Game()
        {
            mapParser = new MapParser();
        }

        public void Play()
        {
            int turn = 0;
            while (turn++ < Constants.MAX_TURNS)
            {
                var map = mapParser.ParseMap();
                map.Turn = turn;
                new ReaperCommander((Reaper)map.Me.Reaper, map).TakeAction();
                new DestroyerCommander((Destroyer)map.Me.Destroyer, map).TakeAction();
                new DoofCommander((Doof)map.Me.Doof, map).TakeAction();

                if(map.IsFinished()) break;
            }
        }
    }
}
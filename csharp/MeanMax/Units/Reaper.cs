namespace MeanMax.Units
{
    public class Reaper : IUnit
    {
        public int Id { get; set; }
        public float Mass { get; set; }
        public int Radius { get; set; }
        public Point Position { get; set; }
        public Point Speed { get; set; }
    }
}
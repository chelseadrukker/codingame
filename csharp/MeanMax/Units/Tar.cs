namespace MeanMax.Units
{
    public class Tar : IUnit
    {
        public int Id { get; set; }
        public int Radius { get; set; }
        public Point Position { get; set; }
        public Point Speed { get; set; }
        public int Duration { get; set; }
    }
}
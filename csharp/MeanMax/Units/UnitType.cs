namespace MeanMax.Units
{
    internal enum UnitType
    {
        Reaper = 0,
        Destroyer = 1,
        Doof = 2,
        Tanker = 3,
        Wreck = 4,
        Tar = 5,
        Oil = 6
    }
}
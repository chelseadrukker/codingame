namespace MeanMax.Units
{
    public interface IUnit
    {
        int Id { get; set; }
        Point Position { get; set; }
        int Radius { get; set; }
        Point Speed { get; set; }
    }
}
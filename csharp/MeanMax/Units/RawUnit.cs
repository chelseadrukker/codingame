﻿namespace MeanMax.Units
{
    internal class RawUnit : IUnit
    {
        public int Id { get; set; }
        public UnitType Type { get; set; }
        public int PlayerId { get; set; }
        public float Mass { get; set; }
        public int Radius { get; set; }
        public Point Position { get; set; }
        public Point Speed { get; set; }
        public int WaterLevelOrDuration { get; set; }
        public int WaterCapacity { get; set; }
    }
}
namespace MeanMax.Units
{
    public class Wreck : IUnit
    {
        public int Id { get; set; }
        public int Radius { get; set; }
        public Point Position { get; set; }
        public Point Speed { get; set; }
        public int WaterLevel { get; set; }
        public int WaterCapacity { get; set; }
    }
}
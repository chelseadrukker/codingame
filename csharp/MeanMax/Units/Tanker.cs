namespace MeanMax.Units
{
    public class Tanker : IUnit
    {
        public int Id { get; set; }
        public float Mass { get; set; }
        public int Radius { get; set; }
        public Point Position { get; set; }
        public Point Speed { get; set; }
        public int WaterLevel { get; set; }
    }
}
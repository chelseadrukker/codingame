﻿using System;
using System.Linq;
using MeanMax.Units;

namespace MeanMax.Commanders
{
    class DestroyerCommander : UnitCommander
    {
        public DestroyerCommander(Destroyer destroyer, Map map) : base(destroyer, map)
        {
        }

        public void TakeAction()
        {
            var actionTaken = UseSkill() || Move();
        }

        private bool UseSkill()
        {
            var target = map.Me.Reaper.Position;
            if (Constants.DESTROYER_USE_SKILL && HasEnoughRage(Constants.GRENADE_COST) && target.IsInside(unit.Position, Constants.SKILL_RANGE))
            {
                DecreaseRage(Constants.GRENADE_COST);
                Console.WriteLine($"SKILL {target.X} {target.Y} DESTROYER");
                return true;
            }
            return false;
        }

        private bool Move()
        {
            var nearestTanker = map.Tankers.OrderByDistanceTo(unit).First()?.Position;
            var destination = nearestTanker ?? map.GetBestOpponent().Reaper.Position;
            Console.WriteLine($"{destination.X} {destination.Y} 300 DESTROYER");
            return true;
        }
    }
}

﻿using System.Collections.Generic;
using System.Linq;
using MeanMax.Units;

namespace MeanMax.Commanders
{
    internal class UnitCommander
    {
        protected readonly IUnit unit;
        protected readonly Map map;
        protected int priority;

        internal UnitCommander(IUnit unit, Map map)
        {
            this.unit = unit;
            this.map = map;
        }

        internal IUnit MyDoof()
        {
            return map.Me.Doof;
        }

        internal IUnit GetNearestEnemyDoof()
        {
            return map.Opponents.Select(o => o.Doof).OrderByDistanceTo(unit).First();
        }

        internal IUnit GetBestEnemyDoof()
        {
            return map.Opponents.OrderByDescending(o => o.Score).First().Doof;
        }

        internal IUnit MyDestroyer()
        {
            return map.Me.Destroyer;
        }

        internal IUnit GetNearestEnemyDestroyer()
        {
            return map.Opponents.Select(o => o.Destroyer).OrderByDistanceTo(unit).First();
        }

        internal IUnit GetBestEnemyDestroyer()
        {
            return map.Opponents.OrderByDescending(o => o.Score).First().Destroyer;
        }

        internal IUnit MyReaper()
        {
            return map.Me.Reaper;
        }

        internal IUnit GetNearestEnemyReaper()
        {
            return map.Opponents.Select(o => o.Reaper).OrderByDistanceTo(unit).First();
        }

        internal IUnit GetBestEnemyReaper()
        {
            return map.Opponents.OrderByDescending(o => o.Score).First().Reaper;
        }

        internal bool HasEnoughRage(int cost)
        {
            return map.Me.Rage >= cost;
        }
        internal void DecreaseRage(int cost)
        {
            map.Me.Rage -= cost;
        }

        internal bool StandingOnWreck()
        {
            return map.Wrecks.Any(wreck => unit.IsInside(wreck));
        }

        internal bool StandingOnOil()
        {
            return map.Oils.Any(oil => unit.IsInside(oil));
        }

        internal void AddMove(List<Move> moves, Point target, string moveName)
        {
            moves.Add(new Move(target, priority++, moveName));
        }
    }
}
﻿using System;
using System.Linq;
using MeanMax.Units;

namespace MeanMax.Commanders
{
    class DoofCommander : UnitCommander
    {
        public DoofCommander(Doof doof, Map map) : base(doof, map)
        {
        }

        public void TakeAction()
        {
            bool actionTaken = LateGame() || Move();
        }

        internal bool Immediate()
        {
            var bestEnemyReaper = GetBestEnemyReaper();
            if (Constants.DOOF_USE_SKILL
                && HasEnoughRage(Constants.OIL_COST)
                && bestEnemyReaper.Position.IsInside(unit.Position, Constants.SKILL_RANGE)
                && map.Wrecks.Any(w => bestEnemyReaper.IsInside(w)))
            {
                DecreaseRage(Constants.OIL_COST);
                Console.WriteLine($"SKILL {bestEnemyReaper.Position.X} {bestEnemyReaper.Position.Y} DOOF");
                return true;
            }
            return false;
        }

        internal bool LateGame()
        {
            var bestEnemyReaper = GetBestEnemyReaper();
            if (Constants.DOOF_USE_SKILL
                && (HasEnoughRage(150) || map.Opponents.Any(o => o.Score >= 48))
                && bestEnemyReaper.Position.IsInside(unit.Position, Constants.SKILL_RANGE)
                && map.Wrecks.Any(w => bestEnemyReaper.IsInside(w)))
            {
                DecreaseRage(Constants.OIL_COST);
                Console.WriteLine($"SKILL {bestEnemyReaper.Position.X} {bestEnemyReaper.Position.Y} DOOF");
                return true;
            }
            return false;
        }

        public bool Move()
        {
            var bestEnemyReaper = GetBestEnemyReaper();
            Console.WriteLine($"{bestEnemyReaper.Position.X} {bestEnemyReaper.Position.Y} 300 DOOF");
            return true;
        }
    }
}

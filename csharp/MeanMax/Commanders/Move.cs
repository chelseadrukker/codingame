﻿namespace MeanMax.Commanders
{
    internal class Move
    {
        public Move( Point target, int priority, string moveName)
        {
            Target = target;
            Priority = priority;
            MoveName = moveName;
        }

        public Point Target { get; private set; }
        public int Priority { get; private set; }
        public string MoveName { get; private set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using MeanMax.Units;

namespace MeanMax.Commanders
{
    class ReaperCommander : UnitCommander
    {
        public ReaperCommander(Reaper unit, Map map) : base(unit, map)
        {
        }

        public void TakeAction()
        {
            var actionTaken = UseSkill() || Move();
        }

        internal bool UseSkill()
        {
            if (Constants.REAPER_USE_SKILL && StandingOnWreck() && !StandingOnOil() && HasEnoughRage(Constants.TAR_COST + 30))
            {
                DecreaseRage(Constants.TAR_COST);
                UseSkillOnPoint(GetNearestEnemyDoof().Position);
                return true;
            }
            return false;
        }

        private bool Move()
        {
            var moves = new List<Move>();
            AddMove(moves, ChillOnWreck(), "Chill");
            AddMove(moves, GetWreckWithMostIntersectionsAndMostWaterFromThem(), $"Best Wreck {GetWreckWithMostIntersectionsAndMostWaterFromThem()}");
            AddMove(moves, MyDestroyer().Position, $"My Destroyer {MyDestroyer().Position}");
            AddMove(moves, new Point(0, 0), "Center Of Map");
            MakeMove(moves.Where(m => m.Target != null).OrderBy(m => m.Priority).First());
            return true;
        }

        private Point ChillOnWreck()
        {
            if (StandingOnWreck() && !StandingOnOil())
                return MyReaper().Position;
            return null;
        }

        private Point GetWreckWithMostIntersectionsAndMostWaterFromThem()
        {
            var destination = map.Wrecks
                .Where(w => !map.Oils.Any(o => w.Position.IsInside(o)))
                .OrderByDescending(wreck => map.Wrecks.Where(otherWreck => wreck.IntersectsWith(otherWreck) && otherWreck.Position.IsInside(wreck)).Sum(w => ((Wreck)w).WaterLevel)).FirstOrDefault();
            if (destination != null)
            {
                var destinations = map.Wrecks.Where(w => w.IntersectsWith(destination));
                var center = destinations.OrderByDescending(d => ((Wreck)d).WaterLevel).First().Position;
                return center;
            }
            return null;
        }

        private void MakeMove(Move move)
        {
            Console.WriteLine($"{move.Target.X - unit.Speed.X} {move.Target.Y - unit.Speed.Y} 300 REAPER {move.MoveName}");
        }

        private void UseSkillOnPoint(Point target)
        {
            Console.WriteLine($"SKILL {target.X} {target.Y} REAPER {target.X} {target.Y}");
        }
    }
}

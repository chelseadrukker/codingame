using System;
using System.Collections.Generic;
using System.Linq;
using MeanMax.Units;

namespace MeanMax
{
    public class MapParser
    {
        public Map ParseMap()
        {
            var map = new Map();
            ParsePlayers(map);
            ParseUnits(map);
            return map;
        }

        private void ParsePlayers(Map map)
        {
            map.Me = new Player { Id = 0 };
            Player opponent1 = new Player { Id = 1 };
            Player opponent2 = new Player { Id = 2 };

            map.Me.Score = int.Parse(Console.ReadLine());
            opponent1.Score = int.Parse(Console.ReadLine());
            opponent2.Score = int.Parse(Console.ReadLine());

            map.Me.Rage = int.Parse(Console.ReadLine());
            opponent1.Rage = int.Parse(Console.ReadLine());
            opponent2.Rage = int.Parse(Console.ReadLine());

            map.Opponents = new List<Player> { opponent1, opponent2 };
        }

        private void ParseUnits(Map map)
        {
            map.Tankers = new List<IUnit>();
            map.Wrecks = new List<IUnit>();
            map.Tars = new List<IUnit>();
            map.Oils = new List<IUnit>();
            int unitCount = int.Parse(Console.ReadLine());
            for (int i = 0; i < unitCount; i++)
            {
                string[] inputs = Console.ReadLine().Split(' ');

                RawUnit rawUnit = ParseUnit(inputs);
                var player = map.Opponents.FirstOrDefault(p => p.Id == rawUnit.PlayerId) ?? map.Me;
                switch (rawUnit.Type)
                {
                    case UnitType.Reaper:
                        {
                            player.Reaper = new Reaper
                            {
                                Id = rawUnit.Id,
                                Mass = rawUnit.Mass,
                                Radius = rawUnit.Radius,
                                Position = rawUnit.Position,
                                Speed = rawUnit.Speed
                            };
                            break;
                        }
                    case UnitType.Destroyer:
                        {
                            player.Destroyer = new Destroyer
                            {
                                Id = rawUnit.Id,
                                Mass = rawUnit.Mass,
                                Radius = rawUnit.Radius,
                                Position = rawUnit.Position,
                                Speed = rawUnit.Speed
                            };
                            break;
                        }
                    case UnitType.Doof:
                        {
                            player.Doof = new Doof
                            {
                                Id = rawUnit.Id,
                                Mass = rawUnit.Mass,
                                Radius = rawUnit.Radius,
                                Position = rawUnit.Position,
                                Speed = rawUnit.Speed
                            };
                            break;
                        }
                    case UnitType.Tanker:
                        {
                            map.Tankers.Add(new Tanker
                            {
                                Id = rawUnit.Id,
                                Mass = rawUnit.Mass,
                                Radius = rawUnit.Radius,
                                Position = rawUnit.Position,
                                Speed = rawUnit.Speed,
                                WaterLevel = rawUnit.WaterLevelOrDuration
                            });
                            break;
                        }
                    case UnitType.Wreck:
                        {
                            map.Wrecks.Add(new Wreck
                            {
                                Id = rawUnit.Id,
                                Radius = rawUnit.Radius,
                                Position = rawUnit.Position,
                                Speed = rawUnit.Speed,
                                WaterLevel = rawUnit.WaterLevelOrDuration,
                                WaterCapacity = rawUnit.WaterCapacity
                            });
                            break;
                        }
                    case UnitType.Tar:
                        {
                            map.Tars.Add(new Tar
                            {
                                Id = rawUnit.Id,
                                Radius = rawUnit.Radius,
                                Position = rawUnit.Position,
                                Speed = rawUnit.Speed,
                                Duration = rawUnit.WaterLevelOrDuration
                            });
                            break;
                        }
                    case UnitType.Oil:
                        {
                            map.Oils.Add(new Oil
                            {
                                Id = rawUnit.Id,
                                Radius = rawUnit.Radius,
                                Position = rawUnit.Position,
                                Speed = rawUnit.Speed,
                                Duration = rawUnit.WaterLevelOrDuration
                            });
                            break;
                        }
                    default:
                        {
                            throw new Exception("Unrecognized unit type");
                        }
                }
            }
        }

        private RawUnit ParseUnit(string[] inputs)
        {
            return new RawUnit
            {
                Id = int.Parse(inputs[0]),
                Type = (UnitType)int.Parse(inputs[1]),
                PlayerId = int.Parse(inputs[2]),
                Mass = float.Parse(inputs[3]),
                Radius = int.Parse(inputs[4]),
                Position = new Point(int.Parse(inputs[5]),
                    int.Parse(inputs[6])),
                Speed = new Point(int.Parse(inputs[7]),
                    int.Parse(inputs[8])),
                WaterLevelOrDuration = int.Parse(inputs[9]),
                WaterCapacity = int.Parse(inputs[10])
            };
        }
    }
}
﻿using System.Collections.Generic;
using System.Linq;
using MeanMax.Units;

namespace MeanMax
{
    public class Map
    {
        public List<Player> Opponents { get; set; }
        public List<IUnit> Tankers { get; set; }
        public List<IUnit> Wrecks { get; set; }
        public List<IUnit> Tars { get; set; }
        public List<IUnit> Oils { get; set; }
        public Player Me { get; set; }
        public int Turn { get; set; }

        public bool Leading()
        {
            return Opponents.All(o => o.Score < Me.Score);
        }

        public Player GetBestOpponent()
        {
            return Opponents.OrderByDescending(o => o.Score).First();
        }
        
        public bool IsFinished()
        {
            return Me.Score == 50 || Opponents.Any(o => o.Score == 50);
        }
    }

}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using MeanMax.Units;

namespace MeanMax
{
    public static class Calculators
    {
        public static Point GetCenterOf(this IEnumerable<Point> points)
        {
            var enumerable = points as Point[] ?? points.ToArray();
            var sunX = enumerable.Sum(d => d.X);
            var sumY = enumerable.Sum(d => d.Y);
            var count = enumerable.Length;
            return new Point(sunX / count, sumY / count); ;
        }

        public static IEnumerable<IUnit> OrderByDistanceTo(this IEnumerable<IUnit> units, IUnit unit)
        {
            return units.OrderBy(u => Calculators.GetDistanceWithoutSqrt(unit.Position, u.Position));
        }

        public static double GetDistanceWithSqrt(Point source, Point target)
        {
            return Math.Sqrt(GetDistanceWithoutSqrt(source, target));
        }

        public static double GetDistanceWithoutSqrt(Point source, Point target)
        {
            return Math.Pow(source.X - target.X, 2) + Math.Pow(source.Y - target.Y, 2);
        }

        public static bool IntersectsWith(this IUnit a, IUnit b)
        {
            return a.IntersectsWith(b.Position, b.Radius);
        }

        public static bool IntersectsWith(this IUnit a, Point center, int radius)
        {
            return Math.Pow(a.Radius - radius, 2) <= Math.Pow(a.Position.X - center.X, 2) + Math.Pow(a.Position.Y - center.Y, 2) &&
                   Math.Pow(a.Position.X - center.X, 2) + Math.Pow(a.Position.Y - center.Y, 2) <= Math.Pow(a.Radius + radius, 2);
        }

        public static bool IsInside(this IUnit unit, IUnit circle)
        {
            return unit.Position.IsInside(circle.Position, circle.Radius);
        }

        public static bool IsInside(this Point point, IUnit circle)
        {
            return point.IsInside(circle.Position, circle.Radius);
        }

        public static bool IsInside(this Point point, Point center, int radius)
        {
            return Math.Pow(point.X - center.X, 2) + Math.Pow(point.Y - center.Y, 2) <
                   Math.Pow(radius, 2);
        }

        public static List<Point> GetPointsInsideACircle(Point center, int radius)
        {
            var points = new List<Point>();
            int x = center.X;
            int y = center.Y;
            for (int i = y - radius; i < y + radius; i++)
            {
                for (int j = x; Math.Pow(j - x, 2) + Math.Pow(i - y, 2) <= Math.Pow(radius, 2); j--)
                {
                    var point = new Point(x, y);
                    points.Add(point);
                }
                for (int j = x + 1; (j - x) * (j - x) + (i - y) * (i - y) <= radius * radius; j--)
                {
                    var point = new Point(x, y);
                    points.Add(point);
                }
            }
            return points;
        }
    }
}

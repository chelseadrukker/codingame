using MeanMax.Units;

namespace MeanMax
{
    public class Player
    {
        public int Id { get; set; }
        public int Score { get; set; }
        public int Rage { get; set; }
        public IUnit Reaper { get; set; }
        public IUnit Destroyer { get; set; }
        public IUnit Doof { get; set; }
    }
}
import sys
import math
import itertools

index = {}

def volume(*d):
    diameters = map(lambda x: x * x * x, d)
    return sum(diameters)

def surface(d):
    return 4 * 3.14 * (d / 2) * (d / 2)

def getOrAdd(key):
    if key in index:
        return index[key]
    index[key] = volume(key)
    return index[key]

def roundToTwoDecimals(x):
    return round(x, 2)

orb_size_min, orb_size_max = [int(i) for i in input().split()]
glowing_size_1, glowing_size_2 = [int(i) for i in input().split()]

glowing_volume = int(volume(glowing_size_1, glowing_size_2))

diameters = range(orb_size_min, orb_size_max + 1)

combinations = itertools.combinations_with_replacement(diameters, 2)

# throw combinations where sum of sparkling sizes is smaller than the smaller glowing size
combinations = filter(lambda c: glowing_size_1 < c[0] + c[1], combinations)

volumes = map(lambda c: (c, getOrAdd(c[0]) + getOrAdd(c[1])), combinations)

results = list(filter(lambda c: glowing_volume == int(c[1]), volumes))
print(results, file=sys.stderr)

spark = results[0][0] if results[0][0][0] != glowing_size_1 or len(results) == 1 else results[1][0]
print(spark[0], spark[1]) if len(results) > 1 else print('VALID') if len(results) == 1 else print('INTERESTING')


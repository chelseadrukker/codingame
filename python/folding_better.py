import sys
print("",sys.stderr)

times = 4
w, h = 23, 4
rows = ['.#.......#.....x...xxxx', 
        '##..x.....####.#.......', 
        '#...xxx....#...#.###.##', 
        '###........#...#....x..']

# times = 15
# w, h = [i for i in [3, 3]]
# rows = ['...', '...', '.x.']

def cells(rows):
    cells=[]
    for y, row in enumerate(rows):
        for x, cell in enumerate(row):
            if cell == "x":
                cells.append((x, y))
    return cells

def neighbours(cell):
    return [(cell[0]-1, cell[1]),
            (cell[0]+1, cell[1]),
            (cell[0], cell[1]-1),
            (cell[0], cell[1]+1)]

def updateisland(cell, island, w, h):
    if cell[0] == 0: island.add('L')
    if cell[1] == 0: island.add('T')
    if cell[0] == w-1: island.add('R')
    if cell[1] == h-1: island.add('B')

def findanddeleteneighbours(cell, cells, w, h, island):
    updateisland(cell, island, w, h)
    cells.remove(cell)
    for n in neighbours(cell):
        if n in cells:
            findanddeleteneighbours(n, cells, w, h, island)
    return island

rules = {}
rules['LT'] = ['C']
rules['T'] = ['C', 'C']
rules['RT'] = ['R', 'L']
rules['L'] = ['C', 'C']
rules['C'] = ['C', 'C', 'C', 'C']
rules['R'] = ['R', 'L', 'R', 'L']
rules['BL'] = ['B', 'T']
rules['B'] = ['B', 'T', 'B', 'T']
rules['BR'] = ['LT', 'RT', 'BL', 'BR']

rules['BT'] = ['BT', 'BT']
rules['LR'] = ['LR', 'LR']

rules['BLT'] = ['BT']
rules['BRT'] = ['BRT', 'BLT']

rules['LRT'] = ['LR']
rules['BLR'] = ['BLR', 'LRT']

rules['BLRT'] = ['BLRT']

###
### Algorithm
###

for r in rows:
    print(r)

cells=cells(rows)

islands=[]
while len(cells) > 0:
    island=set()
    findanddeleteneighbours(cells[0], cells, w, h, island)
    if len(island) == 0: island.add('C')
    islands.append("".join(sorted(island)))

while times > 0:
    newislands = []
    for i in islands:
        newislands = newislands + rules[i]
    islands = newislands
    print(len(islands))
    times = times - 1

print(sorted(islands))
print(len(islands))
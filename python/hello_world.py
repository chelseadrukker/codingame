import sys
import math

RADIUS = 6371

def greatCircle(lat1, lon1, lat2, lon2):
    a = math.sin(lat1) * math.sin(lat2)
    b = math.cos(lat1) * math.cos(lat2) * math.cos(lon2 - lon1)
    c = math.acos(a + b)
    return int(round(RADIUS * c))

def dmsToRadians(dms):
    sign = -1 if dms[0] in ['S', 'W'] else 1
    arcsec = dms[-2:]
    arcmin = dms[-4:-2]
    degree = dms[-6:-4] if dms[0] in ['S', 'N'] else dms[-7:-4]
    return sign * math.radians(int(degree) + int(arcmin) / 60 + int(arcsec) / 3600)

class City:
    def __init__(self, name, latitude, longitude):
        self.name = name
        self.latitude = dmsToRadians(latitude)
        self.longitude = dmsToRadians(longitude)
    
def parseCities(n):
    cities = []
    for i in range(n):
        cities.append(City(*input().split()))
    for i in range(n):
        cities[i].message = input()
    return cities

def getClosestMessages(cities, location):
    distances = list(map(lambda city: (greatCircle(city.latitude, city.longitude, location.latitude, location.longitude), city.message), cities))
    closest = min(distances, key=lambda x: x[0])[0]
    closestDistances = filter(lambda d: d[0] == closest, distances)
    closestMessages = map(lambda d: d[1], closestDistances)
    return ' '.join(closestMessages)

def main():
    n = int(input())  # number of capitals
    m = int(input())  # number of geolocations for which to find the closest capital
    cities = parseCities(n)
    
    for i in range(m):
        travel_geoloc = input()
        location = City('location', *travel_geoloc.split())
        print(getClosestMessages(cities, location))

main()
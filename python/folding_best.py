###
# Functions
###


def blrt(n):
    return 1


def lr(n):
    return 2 ** n


def c(n):
    return 4 ** n


def blt(n):
    return 2 ** (n - 1)


def lt(n):
    return 4 ** (n - 1)


def t(n):
    return 2 ** (2 * n - 1)


def brt(n):
    return (1 / 2) * (2 + 2 ** n)


def br(n):
    return (2 ** n) + 4 ** (n - 1) + 1


def rt(n):
    return 2 ** (n - 2) * (2 + 2 ** n)


def r(n):
    return 2 ** (n - 1) * (2 + 2 ** n)


rules = {
    'blrt': blrt,
    'lr': lr,
    'bt': lr,
    'c': c,
    'blt': blt,
    'lrt': blt,
    'lt': lt,
    't': t,
    'l': t,
    'brt': brt,
    'blr': brt,
    'br': br,
    'rt': rt,
    'bl': rt,
    'r': r,
    'b': r
}


def apply_rules_on_islands(islands, rules, repeat):
    for i in islands:
        yield rules[i](repeat)


def detect_borders(segment, width, heigth):
    if segment[0] == 0:
        yield 'l'
    if segment[1] == 0:
        yield 't'
    if segment[0] == width-1:
        yield 'r'
    if segment[1] == heigth-1:
        yield 'b'


def neighbours(segment, segments):
    return {(segment[0]-1, segment[1]),
            (segment[0]+1, segment[1]),
            (segment[0], segment[1]-1),
            (segment[0], segment[1]+1)}.intersection(segments)


def search_borders(segment, segments, width, heigth):
    borders = set(detect_borders(segment, width, heigth))
    nb = neighbours(segment, segments)
    segments.difference_update(nb)
    for neighbour in nb:
        borders = borders.union(search_borders(
            neighbour, segments, width, heigth))
    return borders


def trace_island(segments, width, heigth):
    island = search_borders(segments.pop(), segments, width, heigth)
    return 'c' if len(island) == 0 else "".join(sorted(island))


def islands(segments, width, heigth):
    while any(segments):
        yield trace_island(segments, width, heigth)


def segments(input_map):
    for y, map_row in enumerate(input_map):
        for x, segment in enumerate(map_row):
            if segment == "#":
                yield (x, y)

###
# Input
###


repeat = 4
width, heigth = 44, 10
input_map = ['###',
             '#.#',
             '###']

###
# Algorithm
###

print(sum(apply_rules_on_islands(
    islands(set(segments(input_map)), width, heigth), rules, repeat)))

import sys
import math

# this have to be refactored 

class Passage:
    def __init__(self, x, y):
        self.x = x
        self.y = y    
    
    def count_neighbours(self, passages):
        self.neighbours = len([p for p in passages if self.are_neighbours(p)])
    
    def are_neighbours(self, other):
        return (other.y == self.y and abs(other.x - self.x) == 1) or (other.x == self.x and abs(other.y - self.y) == 1)

width, height = [int(i) for i in input().split()]
passages = []

for y in range(height):
    line = input()
    passages.extend([Passage(x,y) for x, cell in enumerate(line) if cell == '0'])

for passage in passages:
    passage.count_neighbours(passages)

result = [['#' for x in range(width)] for y in range(height)]

for p in passages:
    result[p.y][p.x] = str(p.neighbours)

for row in result:
    print(''.join(row))

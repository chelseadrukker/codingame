# s=input()
# print(''.join([''.join(t) for t in zip(s[1::2],s[::2])]))


# import re
# n = "100020300001"
# g=re.split('[1-9]', n)

# print(len(list(reversed(sorted(g,key=len)))[0]))


# import re
# i="I am not a good speller. Sometimes I mix up the spelling of words and sometimes I even forget to put periods. Please count the number of words I have misspelled and the number of periods I have missed."
# p=i.count('.')
# g=set(re.split('\W+', i))
# s = "I am not a gud spiler. Sumtimes I micks up teh spiling of wurds and sumetimes I even forgett to poot pariods Pleas cout the numbr of wurds i have mispilled and the numbr of pariods I have missed."
# x=s.count('.')
# y=set(re.split('\W+', s))
# print(len(g.intersection(y)))
# print(p)
# print(x)
# print(str(len(g.intersection(y))) + " " + str(p-x))

# # pow
# print(2**2)
# # sqrt
# print(4**(1/2))

# #regex
# import re
# d={}
# p = re.compile('A|E|I|O|U')
# for m in p.finditer('CODINGAME'):
#     d[m.start()]=m.group()
# print(d)

# line = 'CODINGAME'
# v=['A', 'E', 'I', 'O', 'U']
# #where
# f=list(filter(lambda x: x in v, line))
# #swap
# f=f[1:]+f[:1]
# #reverse
# f=list(reversed(f))

# res=""
# #iterate
# for c in line:
#     res+=c if c not in v else f.pop()
# print(res)


# for i in range(8):
#     print(i, end='')
# print()
import sys
import math

def max_speed(r):
    return math.floor(math.sqrt(math.tan(math.radians(60)) * r * 9.81))

class Racer:
    def __init__(self, name, speed):
        self.name = name
        self.speed = speed

    def turns_completed(self, turn_limits):
        turns_where_stalled = [s for s in turn_limits if s < self.speed]
        return turn_limits.index(turns_where_stalled[0]) if turns_where_stalled else len(turn_limits)

n = int(input())
v = int(input())
turn_limits = []
racers = []

for i in range(n):
    name = chr(ord('a') + i)
    racer = Racer(name, int(input()))
    racers.append(racer)
for i in range(v):
    max = max_speed(int(input()))
    turn_limits.append(max)

print(min(turn_limits))
print('y')

racers = sorted(racers, key=lambda s: (s.turns_completed(turn_limits), s.speed, -ord(s.name)), reverse=True)
for s in racers:
    print(s.name)
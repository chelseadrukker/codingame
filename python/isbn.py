import sys
import math

n = int(input())
invalids = []
for i in range(n):
    isbn = input()
    
    base = isbn[:-1]
    checksum = isbn[-1]

    if not base.isnumeric():
        invalids.append(isbn)
        continue
    base = list(map(int, base))

    if not (checksum.isdigit() or (len(isbn) == 10 and checksum == "X")):
        invalids.append(isbn)
        continue
    checksum = int(checksum) if checksum.isdigit() else 10

    if len(isbn) not in [10, 13]:
        invalids.append(isbn)
        continue

    if len(isbn) == 10:
        intermed = sum([d * (10 - x) for x,d in enumerate(base)])
        rem = intermed % 11
        if (11 - rem == checksum) or rem == checksum == 0:
            continue

    if len(isbn) == 13:
        intermed = sum([x * 1 for x in base[::2]])
        intermed += sum([x * 3 for x in base[1::2]])
        rem = intermed % 10
        if (10 - rem == checksum) or rem == checksum == 0:
            continue

    invalids.append(isbn)

print(f"{len(invalids)} invalid:")
for inv in invalids:
    print(inv)

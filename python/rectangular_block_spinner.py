import sys
import math

size = int(input())
angle = int(input()) % 360
rotation = int(angle / 90) % 4

matrix = []
for i in range(size):
    matrix.append(input().replace(' ', ''))

result_size = size * 2 - 1
result = [[' ' for x in range(result_size)] for y in range(result_size)]

for y in range(size):
    for x in range(size):
        _x = x + y
        _y = size - 1 + y - x
        result[_x][_y] = matrix[x][y]

for x in range(rotation + 1):
    result = reversed(list(zip(*result)))

for row in result:
    print(''.join(row))

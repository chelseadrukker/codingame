import sys
import math
from fractions import gcd

# Auto-generated code below aims at helping you parse
# the standard input according to the problem statement.

x_a, y_a, x_b, y_b = [int(i) for i in input().split()]
n = int(input())
same_color = 1
results = []
for i in range(n):
    a, b, c = [int(j) for j in input().split()]
    divisor = gcd(a, gcd(b, c))
    a /= divisor
    b /= divisor
    c /= divisor
    pole_a = a * x_a + b * y_a + c
    pole_b = a * x_b + b * y_b + c
    
    if (pole_a == 0 or pole_b == 0):
        print("ON A LINE")
        exit()
    
    if ((pole_a, pole_b) in results or (-pole_a, -pole_b) in results):
        continue
    results.append((pole_a, pole_b))

    if (pole_a/abs(pole_a) != pole_b/abs(pole_b)):
        same_color ^= 1

print("YES" if same_color else "NO")

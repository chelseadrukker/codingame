import sys
import math

def next(number, visited):
    visited.append(number)
    r = sum([pow(int(n), 2) for n in str(number)])
    if r in visited:
        return ':)' if r == 1 else ':('
    return next(r, visited)

for i in range(int(input())):
    x = input()
    result = next(x, [])
    print(f"{x} {result}")
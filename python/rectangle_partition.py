import sys
import math
from itertools import combinations, product
from collections import Counter

w, h, count_x, count_y = [int(i) for i in input().split()]
x = [int(i) for i in input().split()]
x.insert(0, 0)
x.append(w)
y = [int(i) for i in input().split()]
y.insert(0, 0)
y.append(h)

x_distances = [c[1] - c[0] for c in combinations(x, 2)]
y_distances = [c[1] - c[0] for c in combinations(y, 2)]

x_common = sorted([x for x in x_distances if x in y_distances])
x_counts = Counter(x_common).values()
y_common = sorted([y for y in y_distances if y in x_distances])
y_counts = Counter(y_common).values()

assert len(x_counts) == len(y_counts)
result = sum([x * y for x, y in zip(x_counts, y_counts)])

print(result)

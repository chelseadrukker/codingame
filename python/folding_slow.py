import sys
sys.setrecursionlimit(300000)

def mirror(list):
    return list[::-1] + list[0:]


def mirroreach(rows):
    return list(map(lambda x: mirror(x), rows))


def mirrorxandy(rows, times):
    for i in range(0, times):
        rows = mirroreach(mirror(rows))
    return rows


def neighbours(cell):
    return [(cell[0]-1, cell[1]),
            (cell[0]+1, cell[1]),
            (cell[0], cell[1]-1),
            (cell[0], cell[1]+1)]


def findanddeleteneighbours(cell, cells):
    cells.remove(cell)
    for n in neighbours(cell):
        if n in cells:
            findanddeleteneighbours(n, cells)
    return


times = 2
w, h = [i for i in [5, 5]]
rows = []
rows.insert(0, '#.#.#')
rows.insert(1, '.....')
rows.insert(2, '#.#.#')
rows.insert(3, '.....')
rows.insert(4, '#.#.#')

cells = []
for y, row in enumerate(mirrorxandy(rows, times)):
    for x, cell in enumerate(row):
        if cell == "#":
            cells.append((x, y))

result = 0

while len(cells) > 0:
    findanddeleteneighbours(cells[0], cells)
    result = result + 1
print(result)
import sys
import math
import bisect


class Info:
    def __init__(self, info):
        infos = info.split(' killed ')
        self.name = infos[0]
        self.victims = infos[1].split(', ')

class Tribute:
    def __init__(self, name):
        self.name = name
        self.killed = []
        self.killer = 'Winner'
    
    def result(self):
        kills = ', '.join(self.killed) if self.killed else 'None'
        return f'Name: {self.name}\nKilled: {kills}\nKiller: {self.killer}'
    
    def kill(self, victims):
        for victim in victims:
            bisect.insort(self.killed, victim)

    def died(self, killer):
        self.killer = killer            

tributes = sorted([Tribute(input()) for i in range(int(input()))], key=lambda t: t.name)

for i in range(int(input())):
    info = Info(input())
    [tribute.kill(info.victims) for tribute in tributes if tribute.name == info.name]
    [tribute.died(info.name) for tribute in tributes if tribute.name in info.victims]

print('\n\n'.join(t.result() for t in tributes))
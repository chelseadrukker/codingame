import sys
import math

MISS = -1
HIT = 0
BUSTED = 1
NEXT = 2

class Player:
    def __init__(self, name):
        self.name = name
        self.shoots = []
        self.score = 0
        self.score_before_turn = 0
        self.shoots_remaining = 3
        self.misses = ''

    def reset_turn(self):
        self.score_before_turn = self.score
        self.shoots_remaining = 3
        self.misses = ''

    def miss(self):
        if self.misses == 'XX':
            penalty = -self.score
        elif self.misses.endswith('X'):
            penalty = -30
        else:
            penalty = -20
        self.misses += 'X'
        return penalty
        
    def hit(self, shot):
        self.misses += 'O'
        return eval(shot)

    def shoot(self):
        self.shoots_remaining -= 1
        shot = self.shoots.pop(0)
        scored = self.hit(shot) if shot != 'X' else self.miss()
        print(f"{self.name} scored {scored}", file=sys.stderr)
        
        self.score = max(0, self.score + scored)
        
        if self.score == 101:
            print(f"{self.name}")
            exit()
        if self.score > 101:
            self.score = self.score_before_turn
            self.shoots_remaining = 0
            
    def on_turn(self):
        while self.shoots_remaining > 0:
            self.shoot()
        self.reset_turn()

n = int(input())
players = []
for i in range(n):
    players.append(Player(input()))
for i in range(n):
    players[i].shoots = input().split()

while True:
    for player in players:
        player.on_turn()

print("winner unknown")
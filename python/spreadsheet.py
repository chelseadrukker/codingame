import sys
import math

cache = {}

def getOrAdd(key):
    if key in cache:
        return cache[key]
    cache[key] = eval(key)
    return cache[key]

_ = 0

def VALUE(a,b):
    return a

def ADD(a,b):
    return a + b

def SUB(a,b):
    return a - b

def MULT(a,b):
    return a * b

n = int(input())

def to_function(arg):
    if(arg[0] == '$'):
        return 'getOrAdd(\'f' + arg[1:] + '()\')'
    return arg

for i in range(n):
    operation, arg_1, arg_2 = input().split()
    arg_1 = to_function(arg_1)
    arg_2 = to_function(arg_2)
    declaration = f"""def f{i}():
    return {operation}({arg_1}, {arg_2})
"""
    exec(declaration)

cache = dict()
for i in range(n):
    eval(f'print(f{i}())')
